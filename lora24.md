---
version: 2
titre: La technologie LoRA débarque dans le 2.4 Ghz
type de projet: Projet de semestre
année scolaire: 2021/2022
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 2
professeurs co-superviseurs:
  - Serge Ayer
  - Nicolas Schroeter
mots-clé: [IoT, Réseaux, Systèmes embarqués]
langue: [F]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.5\textwidth]{lora.png}
\end{center}
```
## Contexte
La technologie de communication LoRA est une technologie trsè populaire dans le monde de l'Internet des Objets. La technologie a été développée il y a maintenant plus de 10 ans pour mettre à disposition une technologie basse consommation et libre d'accès en opérant dans des bandes de fréquence libres sous le GHz. A cause des régulations nationales différentes, les bandes de fréquence utilisées sont différentes (par exemple 915 MHz aux USA et 868 MHz en Europe).

Afin de pouvoir mettre à disposition des utilisateurs une même solution opérant dans le monde entier. Semtech, le fabricant du chipset LoRa, a donc décidé de développé une version du module de communication LoRa opérant dans la bande 2.4 GHz, libre dans le monde entier. Cette solution permet par exemple de développer une solution de suivi de biens (comme des containers) fonctionnant dans le monde entier, ce qui est particulièrement important lorsque que ces biens traversent les frontières et les continents.

## Objectifs
Le but de ce projet est d'investiguer la technologie LoRa dans la bande de fréquence 2.4 GHz, d'un point de vue théorique mais également pratique. Il s'agira d'étudier les caractéristiques de la technologie comme la portée ou la bande passante, et de les comparer à d'autres technologies populaires opérant dans la bande 2.4 GHz, comme la technologie Bluetooth Low Energy. L'utilisation de la technologie pour la localisation d'objets devra également être étudiée et comparée aux autres technologies offrant des possibilités similaires.
Les tâches principales du projet seront:
- l'étude de la technologie LoRA, en particulier dans la bande de fréquence 2.4 GHz.
- la prise en main des outils de développement mis à disposition par Semtech.
- la conception et le développement d'applications de test permettant d'investiguer les caractéristiques de la technologie (portée, débit de données, précision de localisation).
- l'accomplissement de tests et de mesures permettant la comparaison avec d'autres technologies.

## Contraintes
Pour ce projet, les connaissances et motivations suivantes sont un avantage:
- Intérêt et connaissances de base pour les technologies de communication.
- Intérêt et connaissances de base pour la programmation de systèmes embarqués.
